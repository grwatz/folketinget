#!/usr/bin/env python3

# create some lists of folketing members etc
# download from ft.dk and wikipedia.org
# party names and ministers hardcoded below

import urllib.request
from bs4 import BeautifulSoup
import re

out_file = "data/folketing_members.csv"


wiki_link = "https://da.wikipedia.org/wiki/Folketingsmedlemmer_valgt_i_{}"
election_years = [1957, 1960, 1964, 1966, 1968, 1971,
                  1973, 1975, 1977, 1979, 1981, 1984,
                  1987, 1988, 1990, 1994, 1998, 2001,
                  2005, 2007, 2011, 2015, 2019]


wiki_links = [wiki_link.format(year) for year in election_years] +\
        ["https://da.wikipedia.org/wiki/Folketingsmedlemmer_valgt_22._september_1953"]  # noqa

# sort out some dates in this data
datetime_re = re.compile("\d{4}|([123]?\d\.?\s*(januar|februar|marts"  # noqa
                         "|april|maj|juni|juli|august|september|oktober"
                         "|november|december)\s*)",
                         re.IGNORECASE)
# extract the parti
parti_re = re.compile("\w+")  # noqa
parti = ""

# load and parse each of the above wiki links
# and add the members to the member list
with open(out_file, "w") as f:
    for link in wiki_links:
        year = link[-4:]
        print("[scraping] " + link)
        with urllib.request.urlopen(link) as resp:
            soup = BeautifulSoup(resp, "html.parser")
            # the table we want is just below
            # a h2 header
            try:
                potentials = soup.select_one("h2 + table tbody").select("tr")
            except AttributeError:
                print("Cache bug still?! trying something else")
                potentials = soup.select_one("h2 + p + table tbody").select("tr")
                
            for row in potentials:
                entries = row.find_all("td")
                if not entries:
                    continue
                # print ([e.text for e in entries])
                name = entries[0].text
                if len(entries) > 1:
                    if year == "2019":  # don't ask ...
                        parti = entries[2].text[3:]
                    elif year == "2005":
                        parti = entries[2].text
                    else:

                        parti = entries[1].text
                else:
                    parti = "?"

                f.write(name.replace("\n", "").replace(".", "") + ";" +
                        parti.replace("\n", "") + ";" +
                        year + "\n")
