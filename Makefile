# full run
analysis: lda 11_analysis.html

# steps:
scraping: .state/download_state
metadata: data/folketing_members.csv
segment: scraping metadata .state/segmented_state
tidydata: segment models/hashed_lda_ip.vw
lda: tidydata data/nov_tra_res.csv

###########
# data processing

### 01 scraping metadata
data/pdf_list.txt: 01_scrape_v2.py
	python3 01_scrape_v2.py
data/metadata.csv: data/pdf_list.txt

### 02 downloading pdfs
.state/download_state: 02_download_pdf.sh data/pdf_list.txt
	sh 02_download_pdf.sh; touch .state/download_state

### 03 extracting text from pdfs
.state/txt_state: 03_extract.sh .state/download_state
	sh 03_extract.sh; touch .state/txt_state


### metadata
data/folketing_formand.csv: meta1_formand.py
	python3 meta1_formand.py

data/folketing_members.csv: meta2_members.py
	python3 meta2_members.py

##
.state/segmented_state: 04_segment.py 04_plan_segment.sh .state/txt_state data/folketing_members.csv
	sh 04_plan_segment.sh; touch .state/segmented_state

##
data/tidy_metadata.csv: 05_tidytext.R .state/segmented_state data/metadata.csv data/folketing_formand.csv
	rm data/to_udpipe/*; Rscript 05_tidytext.R

##
.state/udpipe: data/tidy_metadata.csv
	rm data/from_udpipe/*; /bin/ls data/to_udpipe | parallel -j4 --bar "./08_tokenize.sh data/to_udpipe/{} data/from_udpipe/{}"; touch .state/udpipe

##
models/hashed_lda_ip.vw: .state/udpipe data/tidy_metadata.csv 06_stopwords.R
	Rscript 06_stopwords.R

models/doc_topic.model: models/hashed_lda_ip.vw
	vw -k -d models/hashed_lda_ip.vw -b 16 --lda 100 --lda_alpha 0.1 --lda_epsilon 0.1 --lda_rho 0.1 -p models/doc_topic.model --readable_model models/word_topic.model --passes 10 --cache_file models/vw.cache --power_t 0.5 --decay_learning_rate 0.5 --holdout_off --minibatch 256 --lda_D `wc -l < models/hashed_lda_ip.vw`

## 10
data/nov_tra_res.csv: models/doc_topic.model 10_parse_vw_lda.R
	Rscript 10_parse_vw_lda.R

#######
# analysis
11_analysis.html: 11_analysis.Rmd data/nov_tra_res.csv
	Rscript -e 'rmarkdown::render("11_analysis.Rmd", "html_document")'

#######
## clean up
clean:
	rm -rf data; rm -rf .state; rm -rf models/; rm textract_errors.log; mkdir data; mkdir data/pdf; mkdir data/txt; mkdir data/segmented; mkdir data/tidy; mkdir data/from_udpipe; mkdir data/vw; mkdir models; mkdir .state; touch .state/visited.txt
